webdav-server
===

The WebDAV server component, meant to run in the
[users-dav](https://git.autistici.org/ai3/docker/apache2-users-dav)
container. Authenticates users against the [webdav-auth authentication
server](https://git.autistici.org/ai3/tools/webdav-server).

It requires Python3, and runs as a FastCGI server (using flup).


