#!/usr/bin/python

from setuptools import setup, find_packages

setup(
    name="ai-webdav",
    version="0.3",
    description="WebDAV serving stack",
    author="Autistici/Inventati",
    author_email="info@autistici.org",
    url="https://git.autistici.org/ai3/tools/webdav-server",
    install_requires=["flup", "wsgidav>=3"],
    setup_requires=[],
    packages=find_packages(),
    package_data={},
    entry_points={
        'console_scripts': [
            'dav.fcgi = ai_webdav.dav_server:main',
        ],
    }
)

