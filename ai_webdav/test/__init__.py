import logging
import os
import socket
import unittest

TEST_FTPNAME = 'user1'
TEST_PASS = 'password1'
TEST_DN = 'dn'
TEST_UID = 19475


class StubBase(object):

    def setUp(self):
        self._stubs = []
        super(StubBase, self).setUp()

    def tearDown(self):
        super(StubBase, self).tearDown()
        for baseref, attr, fn in self._stubs:
            setattr(baseref, attr, fn)

    def stub(self, baseref, attr, fn):
        oldfn = getattr(baseref, attr)
        self._stubs.append((baseref, attr, oldfn))
        setattr(baseref, attr, fn)
