"""FastCGI WebDAV server."""

import contextlib
import datetime
import errno
import logging
import os
import sys

from ai_webdav.client import DavAuthClient

try:
    import raven
    from raven.middleware import Sentry
    has_sentry = True
except ImportError:
    has_sentry = False

from wsgidav.fs_dav_provider import FilesystemProvider
from wsgidav.wsgidav_app import DEFAULT_CONFIG, WsgiDAVApp
from wsgidav.dc.base_dc import BaseDomainController
from wsgidav.dir_browser import WsgiDavDirBrowser
from wsgidav.error_printer import ErrorPrinter
from wsgidav.http_authenticator import HTTPAuthenticator
from wsgidav.request_resolver import RequestResolver


log = logging.getLogger('webdav')


# Authentication is not entirely straightforward, so let's explain it:
# the only fixed parameter we start with is the UNIX user ID. There
# may be more than one FTP account with that ID, so we retrieve their
# list from LDAP once when the program starts, and then we associate
# the right one with the request path on every incoming request.
# Furthermore, we only allow access to accounts whose home directory
# actually exists on the local server.
class Auth(object):

    def __init__(self, authclient):
        self.accounts = authclient.get_accounts()
        if not self.accounts:
            raise Exception('No accounts!')
        log.info('accounts map: %s', str(self.accounts))

    def get_provider_mapping(self):
        return dict((realm, FilesystemProvider(ac['home']))
                    for realm, ac in self.accounts.items()
                    if os.path.exists(ac['home']))


class DomainController(BaseDomainController):

    authclient = None

    def __init__(self, wsgi_app, config):
        super(DomainController, self).__init__(wsgi_app, config)
        self.accounts = self.authclient.get_accounts()
        self.auth_cache = set()

    def __repr__(self):
        return self.__class__.__name__

    def get_domain_realm(self, path_info, environ):
        return self._calc_realm_from_path_provider(path_info, environ)

    def require_authentication(self, realmname, environ):
        return True

    def supports_http_digest_auth(self):
        return False

    def basic_auth_user(self, realmname, username, password, environ):
        if (realmname, username, password) in self.auth_cache:
            return True

        if self._authenticate(realmname, username, password):
            self.auth_cache.add((realmname, username, password))
            return True

        return False

    def _authenticate(self, realmname, username, password):
        # Find the account associated with the given realm.
        account = self.accounts.get(realmname)
        if not account:
            return False

        # Make sure that the username matches. This check enforces the
        # account -> homeDirectory association.
        if username != account['ftpname']:
            log.error('unauthorized access to account %s from user %s',
                      account['ftpname'], username)
            return False

        # Verify the authentication credentials.
        try:
            return (self.authclient.authenticate(account['dn'], password) == 'ok')
        except Exception as e:
            log.error('unexpected exception in authenticate(): %s', e)
            return False


def _abort(code, msg, start_response):
    start_response('%d %s' % (code, msg), [('Content-Type', 'text/html')])
    return ['<h1>%s</h1>' % msg]


def error_middleware(app):
    def error_wrapper(environ, start_response):
        try:
            return app(environ, start_response)
        except EnvironmentError as e:
            # Raise 403 errors on EACCES, let other errors pass through
            # (so we can examine the stack trace).
            if e.errno == errno.EACCES:
                return _abort(403, 'Permission denied', start_response)
            raise
    return error_wrapper


def create_app(config):
    authclient = DavAuthClient(
        config.get('auth_socket', '/var/run/authdav/auth'))

    auth = Auth(authclient)
    DomainController.authclient = authclient

    dav_config = DEFAULT_CONFIG.copy()
    dav_config.update({
        'verbose': config.get('verbose', 1),
        'provider_mapping': auth.get_provider_mapping(),
        'http_authenticator': {
            'domain_controller': 'ai_webdav.dav_server.DomainController',
            'accept_basic': True,
            'accept_digest': False,
            'default_to_digest': False,
        },
        'middleware_stack': [
            # Drop the DebugFilter from the default list.
            ErrorPrinter,
            HTTPAuthenticator,
            WsgiDavDirBrowser,
            RequestResolver,
        ],
        'dir_browser': {
            'enable': True,
            'icon': False,
            'show_user': True,
            'show_logout': True,
            'ms_sharepoint_support': False,
        },
    })

    return WsgiDAVApp(dav_config)


def run_fastcgi(app, config):
    from flup.server.fcgi import WSGIServer
    app = error_middleware(app)
    if has_sentry and config.get('sentry_url'):
        app = Sentry(app, client=raven.Client(config['sentry_url']))
    WSGIServer(app).run()


def main():
    logging.basicConfig()
    config = dict(
        sentry_url=os.getenv('SENTRY_URL'),
        verbose=int(os.getenv('VERBOSE', '0')),
        auth_socket=os.getenv('AUTH_SOCKET', '/var/run/authdav/auth'),
    )
    run_fastcgi(create_app(config), config)


if __name__ == '__main__':
    main()
